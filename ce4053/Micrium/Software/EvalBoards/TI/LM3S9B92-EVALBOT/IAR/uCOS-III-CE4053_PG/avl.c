#include "avl.h"

//insert function includes rc -> resource ceiling
node * insert(node *T, OS_MUTEX *mutex, int rc, int prev_rc)
{
    if(T==NULL)
    {
        T = (node *)OSMemGet((OS_MEM *)&avlPartition,(OS_ERR *)&err);
        //T=(node*)malloc(sizeof(node));
        T->p_mutex=mutex;
        T->p_mutex->resrc_ceiling = rc;
        T->p_mutex->prev_ceiling  = prev_rc;
        T->left=NULL;
        T->right=NULL;
    }
    else
        if(rc > T->p_mutex->resrc_ceiling)        // insert in right subtree
        {
            T->right=insert(T->right,mutex,rc,prev_rc);
            if(BF(T)==-2)
                if(rc > T->right->p_mutex->resrc_ceiling)
                    T=RR(T);
                else
                    T=RL(T);
        }
        else
            if(rc < T->p_mutex->resrc_ceiling)
            {
                T->left=insert(T->left,mutex,rc,prev_rc);
                if(BF(T)==2)
                    if(rc < T->left->p_mutex->resrc_ceiling)
                        T=LL(T);
                    else
                        T=LR(T);
            }
        
        T->ht=height(T);
        
        return(T);
}

node * delete(node *T, OS_MUTEX *mutex, int rc)
{
    node *p;
    
    if(T==NULL)
    {
        return NULL;
    }
    else
        if(rc > T->p_mutex->resrc_ceiling)        // insert in right subtree
        {
            T->right=delete(T->right, mutex, rc);
            if(BF(T)==2)
                if(BF(T->left)>=0)
                    T=LL(T);
                else
                    T=LR(T);
        }
        else
            if(rc<T->p_mutex->resrc_ceiling)
            {
                T->left=delete(T->left, mutex, rc);
                if(BF(T)==-2)    //Rebalance during windup
                    if(BF(T->right)<=0)
                        T=RR(T);
                    else
                        T=RL(T);
            }
            else
            {
                //data to be deleted is found
                if(T->right!=NULL)
                {    //delete its inorder succesor
                    p=T->right;
                    
                    while(p->left!= NULL)
                        p=p->left;
                    
                    T->p_mutex->resrc_ceiling = p->data;
                    T->right = delete(T->right, mutex, rc);
                    
                    if(BF(T)==2)//Rebalance during windup
                        if(BF(T->left)>=0)
                            T=LL(T);
                        else
                            T=LR(T);
                }
                else
                    return(T->left);
            }
    T->ht=height(T);
    return(T);
}
 
int height(node *T)
{
    int lh,rh;
    if(T==NULL)
        return(0);
    
    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;
        
    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;
    
    if(lh>rh)
        return(lh);
    
    return(rh);
}
 
node * rotateright(node *x)
{
    node *y;
    y=x->left;
    x->left=y->right;
    y->right=x;
    x->ht=height(x);
    y->ht=height(y);
    return(y);
}
 
node * rotateleft(node *x)
{
    node *y;
    y=x->right;
    x->right=y->left;
    y->left=x;
    x->ht=height(x);
    y->ht=height(y);
    
    return(y);
}
 
node * RR(node *T)
{
    T=rotateleft(T);
    return(T);
}
 
node * LL(node *T)
{
    T=rotateright(T);
    return(T);
}
 
node * LR(node *T)
{
    T->left=rotateleft(T->left);
    T=rotateright(T);
    
    return(T);
}
 
node * RL(node *T)
{
    T->right=rotateright(T->right);
    T=rotateleft(T);
    return(T);
}
 
int BF(node *T)
{
    int lh,rh;
    if(T==NULL)
        return(0);
 
    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;
 
    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;
 
    return(lh-rh);
}
 
void preorder(node *T)
{
    if(T!=NULL)
    {
        printf("%d(Bf=%d)",T->p_mutex->resrc_ceiling,BF(T));
        preorder(T->left);
        preorder(T->right);
    }
}
 
void inorder(node *T)
{
    if(T!=NULL)
    {
        inorder(T->left);
        printf("%d(Bf=%d)",T->data,BF(T));
        inorder(T->right);
    }
}

int minValue(node* T) {
  struct node* current = T;
 
  /* loop down to find the leftmost leaf */
  while (current->left != NULL) {
    current = current->left;
  }
  return(current->p_mutex->resrc_ceiling);
}