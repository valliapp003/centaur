#include <stdio.h>
#include <stddef.h>
#include <os.h>

extern OS_MEM MemBlock;
extern CPU_INT32U Memory_alloc_init[10][20];
extern CPU_INT32U size;  /* number of nodes in the tree */
           /* Not actually needed for any of the operations */

extern OS_ERR err;

typedef struct tree_node splayTree;
struct tree_node {
    splayTree * left, * right;
    OS_TCB *p_tcb;
    OS_MUTEX   *p_mutex;
};

splayTree * splaytree (CPU_INT32U i, splayTree * t);
//Tree * insert(CPU_INT32U period, CPU_INT32U i, Tree * t, OS_TCB * p_tcb);

splayTree * inserttree(splayTree * t, 
              OS_TCB * p_tcb,OS_MUTEX   *p_mutex);

splayTree * deletetree(splayTree * t, OS_TCB * p_tcb);
int minvalue(splayTree * node);
void preOrder (splayTree *root);